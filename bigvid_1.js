/**
 *  CREATE VIDEO SERVER
 *
 */
const getBigVideo = require('./src/bigvideo')
const videoBroadcaster = require('./lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [

    {roomno: 'A01', port: 9801, path: 'wss://vd1-ali.shoujia0475.com/live/A01_2.flv?token=80c0727a640a2cd10e70de2755c59d1e8e069bfc813220f756a232987a18066ca003675e5ea4139007f481e72ddacc74bf5cf7197f70bad57c9987c6d39bfcddfdea58ee26dcc11010e430031b678e94', id: 'oytmvb9m1zysmc44'}
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getBigVideo(table.roomno, table.path, wss)
    })
}