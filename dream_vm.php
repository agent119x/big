<?php
$roomno = $_GET["m"];
$stream = "";

switch($roomno) {

    case "DG1": $stream = "https://hdl.gzyysc.com/live/dgbc0101spc.flv?wsSecret=4ccf9f9b1334619cd5b75ecd6699f3a5&wsTime=1601738383"; break;
    case "DG2": $stream = "https://hdl.gzyysc.com/live/dgbc0101spc.flv?wsSecret=4ccf9f9b1334619cd5b75ecd6699f3a5&wsTime=1601738383"; break;
    case "DG3": $stream = ""; break;
    case "DG5": $stream = ""; break;
    case "DG6": $stream = ""; break;
    case "DG7": $stream = ""; break;
    case "DG12": $stream = ""; break;
}
?>
<html>
    <head>
        <title>DG</title>
        <script src="https://ogplus.oriental-game.com/static/nodeplayer/NodePlayer.min.js"></script>
    </head>
    <body>
        <style>
            #videoElement {
                position: absolute;
                z-index: -1;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                object-fit: cover;
            }
        </style>

        <canvas id="videoElement"></canvas>

        <script>

        var url = "<?php echo $stream; ?>";
       
        /**
        * 是否打印debug信息
        */
        // NodePlayer.debug(true);

        var player = new NodePlayer();
        
        player.setView("videoElement");

        player.setScaleMode(2);

        player.setKeepScreenOn();

        player.setBufferTime(1000);

        player.on("start", () => {
            console.log("player on start");
        });

        player.on("stop", () => {
            console.log("player on stop");
        });

        player.on("error", e => {
            console.log("player on error", e);
        });

        function startFunc() {
            /**
             * 开始播放,参数为 http-flv或 websocket-flv 的url
             */
            player.start(url);
        };

        startFunc()

        </script>
    </body>
</html>

