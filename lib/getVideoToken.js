/**
 * GET BIG GAMING VIDEO TOKEN
 *
 */
const axios = require('axios')

async function getToken(room) {
    return await axios.get('http://103.31.13.236:1001/big/getVideoToken.asp', {params: {
        roomno: room
    }}).then(res => res.data.token).catch(err => console.log(err))
}

module.exports = getToken