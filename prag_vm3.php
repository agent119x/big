<?php
$roomno = $_GET["m"];
$stream = "";

switch($roomno) {

    case "A01": $stream = "ws://172.104.91.8:9801"; break;
    case "EV2": $stream = "ws://203.245.30.130:9702"; break;
    case "EV3": $stream = "ws://203.245.30.130:9703"; break;
    case "EV4": $stream = "ws://203.245.30.130:9704"; break;
    case "EV5": $stream = "ws://203.245.30.130:9705"; break;
    case "EV6": $stream = "ws://203.245.30.130:9706"; break;
    case "EV7": $stream = "ws://203.245.30.130:9707"; break;
}
?>
<html>
<head>
<script src="http://183.111.227.37/wsPlayer.js"></script>
<style>
    html, body {
        margin: 0;
        padding: 0;
        overflow:hidden;
    }
    #playerDiv {
        background-image: url(../images/movie_placeholder.png);
    }
    #playButton-playerDiv{
        display: none;
    }
</style>
</head>
<body>
<div id="playerDiv" style="background-image: url(../images/movie_placeholder.png);width: 100%; height: 100%; overflow: hidden; position: relative; background-color: black;background-repeat:no-repeat;background-position: center;">
    <video name="vVideo" id="vVideo" autoplay="true" playsinline="true" style="width: 110%;position: relative;left: -5%;top: 0px;" src="">
</video>

<script>
function play() {
        if(wsp) {
            wsp.destroy();
        }
        
        wsp = null;
        wsp = new WSPlayer();
        var v2 = document.querySelector('#vVideo');
        wsp.initVideo(v2);
        wsp.start("<?php echo $stream; ?>");
    }

play();

</script>

</body>
</html>