/**
 *  CREATE VIDEO SERVER
 *
 */
const getBigVideo = require('./src/bigvideo')
const videoBroadcaster = require('./lib/videoBroadcaster') //create video broadcaster server
const cluster = require('cluster')

const workers = 1

const TABLES = [
    {roomno: 'A01', port: 9801, path: 'wss://ws.pragmaticplaylive.net/BC4.1?JSESSIONID=IGvvRr1qvdAnpSH35a6JatZ8UVJNtLmlAgAMS5vlw_49SRaQRued!-1878203830'}
]

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);

    let worker = cluster.fork().process;
    console.log('worker %s started.', worker.pid);

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {

    TABLES.forEach(table => {
        // create websocker server
        let wss = videoBroadcaster(table.roomno, table.port)
        // get video and rebroadcast
        getBigVideo(table.roomno, table.path, wss)
    })
}