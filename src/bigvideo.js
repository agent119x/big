/**
 * GET BIG VIDEO WEBSOCKET
 *
 */
const Websocket = require('ws')

const getVideoToken = require('../lib/getVideoToken')

async function main(room, vidpath, broadcaster) {

   // let videoToken = await getVideoToken(room)
    let interval = null

    //let BIG_VIDEO_URL = `${vidpath}?token=${videoToken}`

    let BIG_VIDEO_URL = vidpath

    const ws = new Websocket(BIG_VIDEO_URL)
    ws.binaryType = 'arraybuffer'

    ws.on('open', () => {
       console.log(`[${room}] Connected to the server!`)

       ws.send('quality0')
    })
    
    ws.on('message', data => {

        if(data instanceof ArrayBuffer) broadcaster.clients.forEach(function each(client) { client.send(data); })
        
        else console.log(`[${room}] ${data}`)    
    })

    ws.on('error', async(err) => {
        console.log(`[${room}] ${err.message}. Restarting...`)
        clearInterval(interval)
        // reset
        main(room, vidpath, broadcaster)
    })
/*
    // get video token every 1 minute
    interval = setInterval( async() => {
        videoToken = await getVideoToken(room)
    }, 1000 * 10); */
}

module.exports = main