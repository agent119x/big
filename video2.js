var VideoComponent = function () {
    function _0x4898dd() {
        this.path = '';
        this.video = null;
        this.videoVolume = 0.6;
        this.codecString = '';
        this.mediaSource = null;
        this.ws = null;
        this.buffer = null;
        this.bufferWorker = null;
        this.initBuffer = new Uint8Array();
        this.videocontainer = null;
        this.activeConnectionTime = +new Date();
        this.isLowBandWidth = false;
        this.msgcnt = 0;
        this.isPlayerStarted = false;
        this.currentChannel = 1;
        this.queue = [];
        this.bandwidthArray = [];
        this.isAutoChannel = true;
        this.isInitBandWidth = false;
        this.clientBandwidth = 0;
        this.skiptoChannel = 0;
        this.bandwidthDownloadSize = [300, 140, 80, 40];
        this.avgbandwidth = 0;
        this.bandwidth = 0;
        this.bandWidthImageUrl = null;
        this.latency = 0;
        this.videoPauseCount = 0;
        this.channelChangeTime = +new Date();
        this.videoPauseCountResetTime = +new Date();
        this.channelUptimer = null;
        this.bwPrintCounter = 0;
        this.isChannelChanging = false;
        this.latencyUpdatedTime = +new Date();
        this.isChannelGoingDown = false;
        this.isFirstInitBuffer = true;
        this.videoWaitingTimer = null;
        this.videoUrlObs = null;
        this.languageModel = null;
        this.videoLoader = null;
        this.loaderpath = null;
        this.videoIconPath = null;
        this.posterUrl = null;
        this.avgInstantBandwidthArray = [];
        this.avgBandwidth = 0;
        this.reconnectingTimer = null;
        this.pingCounter = 0;
        this.pingReturnCounter = 0;
        this.pingTimer = null;
        this.pingTimerTimeout = null;
        this.pingDowngradeCount = 0;
        this.isVideoStopped = false;
        this.isVideoResumeCount = 0;
        this.isVideoPausedinGame = false;
        this.canvas = null;
        this.context = null;
        this.dataUrl = null;
        this.img = null;
        this.isStageIntraction = false;
        this.isVideoPlaybackStarted = false;
        this.playbackStartTime = null;
        this.isAutoRoulette = false;
        this.isAutoRouletteVideoZoom = false;
        this.isYouBoraloaded = false;
        this.countryCode;
        this.videoRestrictToChannel = 0;
        this.pingPongArray = [];
        this.latencyArray = [];
        this.pingPongDelayCheckForUpgrade = 300;
        this.lowLatencyBufferCheckForUpgrade = 1;
        this.isBandWidthDowngradedRecently = false;
        this.isBandWidthDowngradedTimeout = 30000;
    }
    _0x4898dd.prototype.ngOnInit = function () {
        window.parent.window.postMessage({
            'type': 'notifyVideoIframeLoaded'
        }, '*');
        var _0x48fea7 = this;
        this.bindEvent(window, 'message', function (_0x24a9a4) {
            var _0x3eb29b = _0x24a9a4.data;
            switch (_0x3eb29b.type) {
            case 'onWsInit':
                _0x48fea7.checkVideoRestriction(_0x24a9a4.data.countryCode);
                _0x48fea7.ngAfterViewInit(_0x24a9a4.data.path);
                $('#videoavailable div').text(_0x24a9a4.data.noVideoMsg);
                _0x48fea7.posterUrl = _0x24a9a4.data.posterUrl;
                _0x48fea7.img = new Image();
                _0x48fea7.img.src = _0x48fea7.posterUrl + '?n=' + Math.random();
                _0x48fea7.img.onload = () => {
                    $('#videoposterimg').attr('src', _0x48fea7.img.src);
                    _0x48fea7.videoLoader.style.display = 'block';
                };
                if (_0x48fea7.isAutoRoulette) {
                    $('#videoposterimg').css('display', 'block');
                }
                break;
            case 'onChannelChange':
                _0x48fea7.onChannelChanged(_0x24a9a4.data.quality);
                break;
            case 'onVolumeChange':
                if (_0x24a9a4.data.volume == 'NAN') return;
                if (_0x24a9a4.data.volume >= 0) {
                    _0x48fea7.videoVolume = _0x24a9a4.data.volume;
                    if (_0x48fea7.video) {
                        _0x48fea7.video.volume = _0x48fea7.videoVolume;
                        if (_0x48fea7.videoVolume > 0 && _0x48fea7.isStageIntraction) {
                            _0x48fea7.video.muted = false;
                        }
                    }
                }
                break;
            case 'connectSocket':
                _0x48fea7.isVideoPausedinGame = false;
                _0x48fea7.resetplayer();
                _0x48fea7.calculateClientBandwidth();
                break;
            case 'closeSocket':
                if (window.plugin) {
                    plugin.setOptions({
                        'content.rendition': 'videopaused'
                    });
                }
                _0x48fea7.isVideoPausedinGame = true;
                if (_0x48fea7.video) {
                    _0x48fea7.video.pause();
                    if (!_0x48fea7.IsSafari) _0x48fea7.context.drawImage(_0x48fea7.video, 0, 0, 640, 360);
                }
                _0x48fea7.img = new Image();
                _0x48fea7.img.src = _0x48fea7.posterUrl + '?n=' + Math.random();
                _0x48fea7.img.onload = () => {
                    $('#videoposterimg').attr('src', _0x48fea7.img.src);
                    _0x48fea7.videoLoader.style.display = 'none';
                };
                setTimeout(() => {
                    if (_0x48fea7.isVideoPausedinGame) {
                        _0x48fea7.videoLoader.style.display = 'none';
                    }
                }, 2000);
                if (_0x48fea7.ws) {
                    _0x48fea7.ws.close();
                    _0x48fea7.ws.null;
                }
                break;
            case 'onLanguageChange':
                $('#videoavailable div').text(_0x24a9a4.data.noVideoMsg);
                break;
            case 'videoZoomOn':
                _0x48fea7.video.classList.add('zoomin-video');
                break;
            case 'videoZoomOff':
                if (_0x48fea7.video) {
                    _0x48fea7.video.classList.remove('zoomin-video');
                }
                break;
            case 'onInitAutoRoulette':
                _0x48fea7.isAutoRoulette = true;
                _0x48fea7.isAutoRouletteVideoZoom = true;
                break;
            case 'changeAutoRouletteVideo':
                _0x48fea7.isAutoRouletteVideoZoom = _0x24a9a4.data.isZoomIn;
                if (_0x48fea7.isAutoRoulette) {
                    if (_0x48fea7.isAutoRouletteVideoZoom) {
                        window.parent.document.getElementById('videocontainer').style.borderRadius = '50%';
                        window.parent.document.getElementById('iframe-container').classList.add('zoomin-autorla-video');
                        if (_0x48fea7.video) {
                            _0x48fea7.video.classList.add('auto-zoomout-video');
                        }
                        _0x48fea7.canvas.style.display = 'none';
                        document.getElementById('videoposterimg').classList.add('auto-zoomout-videoposter');
                    } else {
                        window.parent.document.getElementById('videocontainer').style.borderRadius = '0px';
                        window.parent.document.getElementById('iframe-container').classList.remove('zoomin-autorla-video');
                        if (_0x48fea7.video) {
                            _0x48fea7.video.classList.remove('auto-zoomout-video');
                        }
                        _0x48fea7.canvas.style.display = 'block';
                        document.getElementById('videoposterimg').classList.remove('auto-zoomout-videoposter');
                    }
                }
                break;
            case 'loadYouBora':
                if (_0x24a9a4.data && _0x24a9a4.data.playerId) {
                    _0x48fea7.loadYouBora(_0x24a9a4.data.playerId, _0x24a9a4.data.casinoId, _0x24a9a4.data.tableId, _0x24a9a4.data.isProd, _0x24a9a4.data.src, _0x24a9a4.data.title);
                }
                break;
            case 'classicViewStatus':
                if (_0x24a9a4.data.isClassicView && _0x48fea7.video) {
                    if (_0x48fea7.video && _0x48fea7.video.classList) {
                        if (_0x24a9a4.data.gameType === 'sicbo') {
                            _0x48fea7.video.classList.add('sicbo-video-zoom-inclassicview');
                            _0x48fea7.video.classList.add('video-border-radius');
                        } else {
                            _0x48fea7.video.classList.add('video-border-radius');
                        }
                    }
                } else if (_0x48fea7.video) {
                    if (_0x24a9a4.data.gameType === 'sicbo') {
                        _0x48fea7.video.classList.remove('sicbo-video-zoom-inclassicview');
                        _0x48fea7.video.classList.remove('video-border-radius');
                    } else {
                        _0x48fea7.video.classList.remove('video-border-radius');
                    }
                }
                break;
            }
        });
    };
    _0x4898dd.prototype.bindEvent = function (_0x338e05, _0x48480b, _0xbd8dfe) {
        if (_0x338e05.addEventListener) {
            _0x338e05.addEventListener(_0x48480b, _0xbd8dfe, false);
        } else if (_0x338e05.attachEvent) {
            _0x338e05.attachEvent('on' + _0x48480b, _0xbd8dfe);
        }
    };
    _0x4898dd.prototype.checkVideoRestriction = function (_0x28af33) {
        var _0x18e87b = this;
        $.getJSON('./countrybasedVideoQuality.json', function (_0x2b6418) {
            var _0x3c211c = JSON.parse(JSON.stringify(_0x2b6418));
            _0x3c211c = _0x3c211c.restrictCountriesData;
            for (var _0x45e701 in _0x3c211c) {
                if (_0x3c211c.hasOwnProperty(_0x45e701)) {
                    var _0x59af5f = _0x3c211c[_0x45e701];
                    if (_0x59af5f.country_code && _0x28af33) {
                        if (_0x59af5f.country_code.toUpperCase().trim() == _0x28af33.toUpperCase().trim()) {
                            _0x18e87b.videoRestrictToChannel = parseInt(_0x59af5f.top_quality);
                            if (!_0x18e87b.videoRestrictToChannel) {
                                _0x18e87b.videoRestrictToChannel = 0;
                            } else if (_0x18e87b.videoRestrictToChannel > 3) {
                                _0x18e87b.videoRestrictToChannel = 3;
                            }
                        }
                    }
                }
            }
        });
    };
    _0x4898dd.prototype.ngAfterViewInit = function (_0x167e72) {
        this.codecString = 'video/mp4; codecs="avc1.640028, mp4a.40.2"';
        this.initBuffer = new Uint8Array();
        this.videocontainer = document.getElementById('videocontainer');
        this.video = document.getElementById('video');
        this.video.controls = false;
        this.video.muted = true;
        this.video.hidden = false;
        this.videoLoader = document.getElementById('videoLoader');
        this.videoLoader.style.display = 'block';
        if (this.isAutoRoulette) {
            $('#videoposterimg').css('display', 'block');
        }
        this.canvas = document.getElementById('videoposter');
        this.canvas.width = 640;
        this.canvas.height = 360;
        this.context = this.canvas.getContext('2d');
        var _0x532e89, _0x4b318f;
        if (typeof document.hidden !== 'undefined') {
            _0x532e89 = 'hidden';
            _0x4b318f = 'visibilitychange';
        } else if (typeof document.msHidden !== 'undefined') {
            _0x532e89 = 'msHidden';
            _0x4b318f = 'msvisibilitychange';
        } else if (typeof document.webkitHidden !== 'undefined') {
            _0x532e89 = 'webkitHidden';
            _0x4b318f = 'webkitvisibilitychange';
        }
        document.addEventListener(_0x4b318f, this.handleVisibilityChange.bind(this));
        if (_0x167e72) {
            this.path = _0x167e72;
            this.bandWidthImageUrl = 'https://ios.pragmaticplaylive.net/images_video/video_bandwidth_512kb.jpg';
            var _0xb8556e = this;
            this.calculateClientBandwidth();
            var _0xb8556e = this;
            this.channelUptimer = setInterval(function () {
                _0xb8556e.checkForChannelUp();
            }, 60000);
        } else {
            if (this.channelUptimer) {
                clearInterval(this.channelUptimer);
                this.channelUptimer = null;
            }
            window.parent.window.postMessage({
                'type': 'notifyClassicViewChange',
                'visible': true
            }, '*');
            $('#videoavailable').show();
            if (this.reconnectingTimer) {
                clearInterval(this.reconnectingTimer);
                this.reconnectingTimer = null;
            }
            this.reconnectingTimer = setInterval(function () {
                if (!_0xb8556e.ws) _0xb8556e.initWS();
            }, 30000);
        }
    };
    _0x4898dd.prototype.handleVisibilityChange = function () {
        console.log(document.hidden, this.isVideoPausedinGame);
        if (!document.hidden && this.video && !this.isVideoPausedinGame) {
            if (this.videoVolume > 0 && this.video.muted) {
                this.video.muted = false;
            }
            if (this.video.currentTime > 20) {
                var _0x44ee8c = this;
                setTimeout(function () {
                    if (!_0x44ee8c.isVideoPlaying()) {
                        if (_0x44ee8c.ws) {
                            _0x44ee8c.ws.close();
                        }
                        _0x44ee8c.resetplayer();
                        _0x44ee8c.calculateClientBandwidth();
                    }
                }, 4000);
            }
        } else if (document.hidden && !this.isVideoPausedinGame) {
            if (this.videoVolume > 0 && !this.video.muted) {
                this.video.muted = true;
            }
        }
    };
    _0x4898dd.prototype.ngOnDestroy = function () {
        this.videoUrlObs.unsubscribe();
        if (this.ws) {
            this.ws.close();
        }
    };
    _0x4898dd.prototype.initWebsocketPlayer = function () {
        if ('MediaSource' in window && MediaSource.isTypeSupported(this.codecString)) {
            this.mediaSource = new MediaSource();
            this.video.src = this.createObjectURL(this.mediaSource);
            this.mediaSource.addEventListener('sourceopen', this.sourceBufferHandle.bind(this));
            this.mediaSource.addEventListener('sourceended', this.onSourceEnded.bind(this));
            this.mediaSource.addEventListener('sourceclose', function (_0x561b0e) {} ['bind'](this));
            this.mediaSource.addEventListener('error', function (_0x2cb8b6) {
                console.log('error: ' + this.mediaSource);
            } ['bind'](this));
        } else {
            console.error('Unsupported MIME type or codec: ', this.codecString);
        }
        var _0x271394 = this;
        window.addEventListener('beforeunload', function (_0x533904) {
            if (_0x271394.ws) {
                _0x271394.ws.close();
                _0x271394.ws = null;
            }
        });
        if (this.pingTimer) {
            clearInterval(this.pingTimer);
            this.pingTimer = null;
        }
        this.pingTimer = setInterval(function () {
            if (_0x271394.isVideoPausedinGame || !_0x271394.isVideoStarted) return;
            var _0x3be68c = +new Date();
            var _0x3ab66d = {
                'cmd': 'ping',
                'counter': _0x271394.pingCounter,
                'clientTime': _0x3be68c
            };
            if (_0x271394.ws) {
                if (_0x271394.ws.readyState == 1) {
                    _0x271394.ws.send(JSON.stringify(_0x3ab66d));
                } else if (_0x271394.ws.readyState > 1) {
                    window.parent.window.postMessage({
                        'type': 'notifyClassicViewChange',
                        'visible': true
                    }, '*');
                    _0x271394.resetplayer();
                    _0x271394.calculateClientBandwidth();
                }
            } else {
                window.parent.window.postMessage({
                    'type': 'notifyClassicViewChange',
                    'visible': true
                }, '*');
                _0x271394.resetplayer();
                _0x271394.calculateClientBandwidth();
            }
            if (_0x271394.pingTimerTimeout) {
                clearTimeout(_0x271394.pingTimerTimeout);
                _0x271394.pingTimerTimeout = null;
            }
            _0x271394.pingTimerTimeout = setTimeout(function () {
                if (_0x271394.pingCounter != _0x271394.pingReturnCounter && !_0x271394.isBandWidthDowngradedRecently) {
                    _0x271394.isBandWidthDowngradedRecently = true;
                    setTimeout(() => {
                        _0x271394.isBandWidthDowngradedRecently = false;
                    }, _0x271394.isBandWidthDowngradedTimeout);
                    _0x271394.pingCounter = _0x271394.pingCounter + 1;
                    _0x271394.pingDowngradeCount++;
                    if (_0x271394.pingCounter > 2 && _0x271394.msgcnt > 1) {
                        if (_0x271394.currentChannel == 3) {
                            _0x271394.isVideoResumeCount = 0;
                            window.parent.window.postMessage({
                                'type': 'notifyClassicViewChange',
                                'visible': true
                            }, '*');
                            _0x271394.video.pause();
                            _0x271394.latencyUpdatedTime = +new Date();
                            window.parent.window.postMessage({
                                'type': 'notifyVideoLatency',
                                'latency': 2500
                            }, '*');
                            _0x271394.resetplayer();
                            _0x271394.initWebsocketPlayer();
                        } else {
                            if (_0x271394.video && !_0x271394.IsSafari) _0x271394.context.drawImage(_0x271394.video, 0, 0, 640, 360);
                            _0x271394.img = new Image();
                            _0x271394.img.src = _0x271394.posterUrl + '?n=' + Math.random();
                            _0x271394.img.onload = () => {
                                _0x271394.videoLoader.style.display = 'block';
                                $('#videoposterimg').attr('src', _0x271394.img.src);
                            };
                            if (_0x271394.isAutoRoulette) {
                                $('#videoposterimg').css('display', 'block');
                            }
                            _0x271394.video.hidden = true;
                            _0x271394.isChannelGoingDown = true;
                            _0x271394.isChannelChanging = true;
                            setTimeout(function () {
                                _0x271394.checkForBandwidthToDowngrade();
                            }, 2000);
                        }
                    }
                } else {
                    _0x271394.pingDowngradeCount = 0;
                    _0x271394.pingCounter = _0x271394.pingCounter + 1;
                    _0x271394.isVideoResumeCount++;
                    if (_0x271394.isVideoResumeCount > 2 && _0x271394.isVideoStopped) {
                        _0x271394.isVideoStopped = false;
                        _0x271394.isVideoResumeCount = 0;
                        _0x271394.playVideo();
                    }
                }
            }, 2000);
        }, 5000);
        this.video.addEventListener('waiting', function () {
            if (!_0x271394.video) return;
            _0x271394.videoLoader.style.display = 'block';
            if (_0x271394.isAutoRoulette) {
                $('#videoposterimg').css('display', 'block');
            }
        });
        this.video.addEventListener('playing', function () {
            if (!_0x271394.video) return;
            _0x271394.videoLoader.style.display = 'none';
            if (_0x271394.isAutoRoulette) {
                $('#videoposterimg').css('display', 'none');
            }
            $('#videoavailable').hide();
        });
        this.video.addEventListener('error', function (_0x4c07f9) {
            console.error(_0x4c07f9, 'video error');
        });
        window.parent.addEventListener('mousedown', function (_0x570c21) {
            _0x271394.onStageTouch();
        } ['bind'](_0x271394));
        window.parent.addEventListener('touchstart', function (_0x4566c2) {
            _0x271394.onStageTouch();
        } ['bind'](_0x271394));
        if (_0x271394.isAutoRoulette) {
            if (_0x271394.isAutoRouletteVideoZoom) {
                _0x271394.canvas.style.display = 'none';
                window.parent.document.getElementById('videocontainer').style.borderRadius = '50%';
                window.parent.document.getElementById('iframe-container').classList.add('zoomin-autorla-video');
                _0x271394.video.classList.add('auto-zoomout-video');
                document.getElementById('videoposterimg').classList.add('auto-zoomout-videoposter');
            }
        }
    };
    _0x4898dd.prototype.onStageTouch = function () {
        if (this.video) {
            this.isStageIntraction = true;
            if (this.video.muted && this.videoVolume > 0) {
                this.video.muted = false;
            }
            if (this.video.paused && !this.isVideoPlaying() && this.isInitBandWidth) {
                if (this.video.seekable.length > 0) {
                    if (this.video.seekable.end >= 1 && !this.isVideoPausedinGame) {
                        this.playVideo();;
                    }
                }
            }
        }
    };
    _0x4898dd.prototype.IsSafari = function () {
        var _0x36aa85 = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        return _0x36aa85;
    };
    _0x4898dd.prototype.sourceBufferHandle = function () {
        if (!this.ws) {
            this.initWS();
        }
        this.buffer = this.mediaSource.addSourceBuffer(this.codecString);
        this.buffer.mode = 'sequence';
        var _0x20e211 = this;
        this.buffer.addEventListener('update', function () {
            _0x20e211.updateBuffer();
        });
        this.buffer.addEventListener('updateend', function () {
            _0x20e211.updateBuffer();
            if (_0x20e211.video.seekable.length > 0) {
                if (_0x20e211.video.seekable.end >= 5 && _0x20e211.video.currentTime === 0) {
                    _0x20e211.video.currentTime = _0x20e211.video.seekable.end - 1;
                }
            }
            if (!_0x20e211.isVideoPlaying()) {
                if (_0x20e211.video.seekable.length > 0) {
                    if (_0x20e211.video.seekable.end >= 1) {
                        if (_0x20e211.video.paused) _0x20e211.playVideo();
                        if (!_0x20e211.isVideoStarted) {
                            _0x20e211.isVideoStarted = true;
                            window.parent.window.postMessage({
                                'type': 'notifyVideoLoaded'
                            }, '*');
                        }
                        if (_0x20e211.videoVolume > 0 && _0x20e211.isStageIntraction) {
                            _0x20e211.video.muted = false;
                        }
                    }
                }
            }
        });
        this.isPlayerStarted = true;
    };
    _0x4898dd.prototype.playVideo = function () {
        var _0x4635e4 = this.video.play();
        if (_0x4635e4 !== undefined) {
            _0x4635e4.then(_0x9d9d7e => {
                this.video.play();
            }).catch(_0x694b18 => {});
        } else {}
    };
    _0x4898dd.prototype.checkForChannelUp = function () {
        if (this.isAutoChannel) {
            if (this.currentChannel > 0 && !this.isChannelChanging && +new Date() - this.activeConnectionTime < 10000 && this.currentChannel > this.videoRestrictToChannel) {
                this.calculateUpBandwidth();
            }
        }
    };
    _0x4898dd.prototype.onSourceEnded = function () {
        this.resetplayer();
        this.calculateClientBandwidth();
    };
    _0x4898dd.prototype.initWS = function () {
        try {
            if (this.isVideoPausedinGame) return;
            if (this.reconnectingTimer) {
                clearInterval(this.reconnectingTimer);
                this.reconnectingTimer = null;
            }
            this.ws = new WebSocket(this.path);
            this.ws.binaryType = 'arraybuffer';
            $('#videoavailable').hide();
            var _0x1eecd1 = this;
            this.ws.onopen = function () {
                console.info('WebSocket connection initialized in channel', _0x1eecd1.currentChannel, +new Date());
            };
            this.ws.addEventListener('message', this.onWebSocketMessage.bind(this));
            this.ws.onclose = function (_0x472918) {
                if (_0x1eecd1.isVideoPausedinGame) return;
                var _0x3f6bf7 = +new Date() - _0x1eecd1.activeConnectionTime;
                if (!_0x1eecd1.ws && _0x3f6bf7 > 30000) {
                    _0x1eecd1.resetplayer();
                    _0x1eecd1.calculateClientBandwidth();
                }
                if (!_0x1eecd1.isVideoStarted) {
                    _0x1eecd1.ws = null;
                    if (_0x1eecd1.channelUptimer) {
                        clearInterval(_0x1eecd1.channelUptimer);
                        _0x1eecd1.channelUptimer = null;
                    }
                    window.parent.window.postMessage({
                        'type': 'notifyClassicViewChange',
                        'visible': true
                    }, '*');
                    $('#videoavailable').show();
                    if (_0x1eecd1.reconnectingTimer) {
                        clearInterval(_0x1eecd1.reconnectingTimer);
                        _0x1eecd1.reconnectingTimer = null;
                    }
                    _0x1eecd1.reconnectingTimer = setInterval(function () {
                        if (!_0x1eecd1.ws) _0x1eecd1.initWS();
                    }, 30000);
                }
            };
            $('#videoavailable').hide();
        } catch (_0x2a613e) {
            if (this.channelUptimer) {
                clearInterval(this.channelUptimer);
                this.channelUptimer = null;
            }
            window.parent.window.postMessage({
                'type': 'notifyClassicViewChange',
                'visible': true
            }, '*');
            $('#videoavailable').show();
            if (this.reconnectingTimer) {
                clearInterval(this.reconnectingTimer);
                this.reconnectingTimer = null;
            }
            this.reconnectingTimer = setInterval(function () {
                if (!_0x1eecd1.ws) _0x1eecd1.initWS();
            }, 30000);
        }
    };
    _0x4898dd.prototype.onWebSocketMessage = function (_0x2fa4c9) {
        if (typeof _0x2fa4c9.data === 'object') {
            if (this.isVideoStopped) {
                return;
            }
            this.activeConnectionTime = this.getCurrentTimeStamp();
            if (this.isChannelGoingDown && this.isVideoPausedinGame && !this.buffer) {
                return;
            }
            this.calculateBandwidth(_0x2fa4c9.data.byteLength);
            this.isLowBandWidth = false;
            var _0x23c173 = new Uint8Array(_0x2fa4c9.data);
            var _0x470513 = _0x23c173[0] == 0 && _0x23c173[1] == 0 && _0x23c173[2] == 0;
            if (this.msgcnt == 0 && _0x470513) {
                this.initBuffer = new Uint8Array(_0x23c173);
                if (this.buffer) {
                    this.buffer.abort();
                    if (!this.buffer.updating) {
                        this.buffer.appendBuffer(this.initBuffer);
                    } else {
                        this.queue.push(this.initBuffer);
                    }
                }
                this.playbackStartTime = +new Date();
                this.msgcnt++;
            } else if (this.msgcnt == 1 && _0x470513 && this.buffer) {
                if (!this.buffer.updating) {
                    this.buffer.appendBuffer(_0x23c173);
                } else {
                    this.queue.push(_0x23c173);
                }
                this.msgcnt++;
            } else if (this.msgcnt > 1 && this.buffer) {
                if (this.buffer.updating || this.queue.length > 0) {
                    this.queue.push(_0x23c173);
                    this.video.hidden = false;
                } else {
                    this.buffer.appendBuffer(_0x23c173);
                    if (this.video.seekable.length > 0) {
                        if (this.video.seekable.end >= 1 && !this.isVideoPlaying()) {
                            if (this.video.paused) this.playVideo();
                            this.video.hidden = false;
                            if (this.videoVolume > 0 && this.isStageIntraction && this.video.muted) {
                                this.video.muted = false;
                            }
                        }
                    }
                    if (this.isVideoPlaybackStarted == false && this.isVideoPlaying()) {
                        this.isVideoPlaybackStarted = true;
                    } else if (!this.isVideoPlaying() && !this.isVideoPlaybackStarted && +new Date() - this.playbackStartTime > 5000) {
                        if (this.videoRestrictToChannel > 0) {
                            if (this.videoRestrictToChannel > this.currentChannel) {
                                this.currentChannel = this.videoRestrictToChannel;
                            }
                        }
                        this.resetplayer();
                        this.initWebsocketPlayer();
                        this.isInitBandWidth = true;
                        this.isVideoPlaybackStarted = true;
                        setTimeout(() => {
                            this.isVideoPlaybackStarted = false;
                        }, 5000);
                    }
                }
                this.msgcnt++;
            } else {}
        } else {
            var _0x2c1a85 = JSON.parse(_0x2fa4c9.data);
            switch (_0x2c1a85.cmd) {
            case 'reset':
                this.currentChannel = _0x2c1a85.ch;
                this.isChannelChanging = false;
                this.isChannelGoingDown = false;
                this.video.pause();
                this.msgcnt = 0;
                this.isFirstInitBuffer = true;
                if (this.currentChannel == 3) {
                    window.parent.window.postMessage({
                        'type': 'notifyClassicViewChange',
                        'visible': true
                    }, '*');
                } else {
                    window.parent.window.postMessage({
                        'type': 'notifyClassicViewChange',
                        'visible': false
                    }, '*');
                }
                break;
            case 'connected':
                if (this.ws) {
                    if (this.videoRestrictToChannel > 0) {
                        if (this.videoRestrictToChannel > this.currentChannel) {
                            this.currentChannel = this.videoRestrictToChannel;
                        }
                    }
                    this.ws.send('quality' + this.currentChannel);
                    this.sendBitrateToYouboraPlugin();
                } else {
                    this.initWS();
                }
                break;
            case 'low_bandwidth_error':
                this.isLowBandWidth = true;
                break;
            case 'ts':
                break;
            case 'pong':
                this.pingReturnCounter = _0x2c1a85.counter;
                var _0x90a5d3 = _0x2c1a85.clientTime;
                var _0x38e97c = +new Date();
                var _0xa0b636 = parseInt(_0x38e97c - _0x90a5d3);
                this.pingPongArray.push(_0xa0b636);
                if (this.pingPongArray.length > 10) {
                    this.pingPongArray.shift();
                }
                this.latencyArray.push(this.latency);
                if (this.latencyArray.length > 10) {
                    this.latencyArray.shift();
                }
                break;
            }
        }
    };
    _0x4898dd.prototype.createObjectURL = function (_0x5a1c35) {
        return window.URL ? window.URL.createObjectURL(_0x5a1c35) : window.webkitURL.createObjectURL(_0x5a1c35);
    };
    _0x4898dd.prototype.calculateUpBandwidth = function () {
        var _0x310198 = this.getChannelForUpgrade(this.currentChannel);
        var _0x15765e = +new Date();
        var _0x43c063 = _0x15765e - this.channelChangeTime;
        if (this.currentChannel > _0x310198 && _0x43c063 > 1.2e+5) {
            this.currentChannel = _0x310198;
            this.ShiftChannel();
        }
    };
    _0x4898dd.prototype.calculateClientBandwidth = function () {
        var _0x4fe907 = this;
        var _0x4d375e = this.bandWidthImageUrl + '?n=' + Math.random();
        var _0x17b8dd, _0x4afc96;
        var _0x474f2a =  0.5 * 0x400 * 0x400;
        var _0x4391d8 = new XMLHttpRequest();
        var _0x1ee629 = false;
        _0x4391d8.open('GET', _0x4d375e, true);
        _0x4391d8.responseType = 'arraybuffer';
        _0x4391d8.timeout = 4000;
        _0x4391d8.ontimeout = function () {
            if (_0x4fe907.isInitBandWidth) return;
            _0x4afc96 = new Date().getTime();
            var _0x11c776 = (_0x4afc96 - _0x17b8dd) / 1000;
            _0x4fe907.currentChannel = 2;
            _0x4fe907.initWebsocketPlayer();
            _0x4fe907.isInitBandWidth = true;
        };
        _0x4391d8.onprogress = function (_0xada77d) {};
        _0x4391d8.onloadend = function (_0xeb147) {
            if (_0x4391d8.status == 200) {
                if (_0x4fe907.isInitBandWidth) return;
                _0x4afc96 = new Date().getTime();
                var _0x346542 = (_0x4afc96 - _0x17b8dd) / 1000;
                var _0x377a12 = _0x474f2a;
                var _0x1ccc9d = Math.round(_0x377a12 / _0x346542);
                var _0xffe430 = _0x1ccc9d / 1024;
                var _0x28d201 = (_0xffe430 / 1024).toFixed(2);
                _0x4fe907.clientBandwidth = _0xffe430;
                _0x4fe907.skiptoChannel = _0x4fe907.getChannel(_0x4fe907.currentChannel, 'up');
                _0x4fe907.currentChannel = _0x4fe907.skiptoChannel;
                _0x4fe907.initWebsocketPlayer();
                _0x4fe907.isInitBandWidth = true;
            } else {
                if (_0x4fe907.isInitBandWidth) return;
                _0x4fe907.currentChannel = 2;
                _0x4fe907.initWebsocketPlayer();
                _0x4fe907.isInitBandWidth = true;
            }
        };
        _0x4391d8.onerror = function (_0x42c8b8) {
            _0x4fe907.currentChannel = 2;
            _0x4fe907.initWebsocketPlayer();
            _0x4fe907.isInitBandWidth = true;
        };
        _0x4391d8.send();
        _0x17b8dd = new Date().getTime();
    };
    _0x4898dd.prototype.extractHostname = function (_0x10733f) {
        var _0x43065d;
        if (_0x10733f.indexOf('//') > -1) {
            _0x43065d = _0x10733f.split('/')[2];
        } else {
            _0x43065d = _0x10733f.split('/')[0];
        }
        _0x43065d = _0x43065d.split(':')[0];
        _0x43065d = _0x43065d.split('?')[0];
        return _0x43065d;
    };
    _0x4898dd.prototype.calculateInstantAvgBandwidth = function (_0x5d9d2c) {
        if (this.avgInstantBandwidthArray.length > 20) {
            this.avgInstantBandwidthArray.shift();
        }
        var _0x21d54a = (+new Date() - this.activeConnectionTime) / 1000;
        if (_0x21d54a > 0 && _0x5d9d2c > 5000) {
            var _0xd5fdda = _0x5d9d2c / _0x21d54a;
            this.avgInstantBandwidthArray.push(_0xd5fdda);
        }
        var _0xc8969e = 0;
        for (var _0x4d7bf4 = 0; _0x4d7bf4 < this.avgInstantBandwidthArray.length; _0x4d7bf4++) {
            _0xc8969e += this.avgInstantBandwidthArray[_0x4d7bf4];
        }
        if (this.avgInstantBandwidthArray.length != 0) {
            this.avgBandwidth = _0xc8969e / this.avgInstantBandwidthArray.length / 1000;
        }
        if (this.bwPrintCounter % 20 == 0) {}
        this.activeConnectionTime = this.getCurrentTimeStamp();
    };
    _0x4898dd.prototype.updateBuffer = function () {
        if (!this.buffer) return;
        this.isPlayerStarted = true;
        this.videoWaitingTimer = null;
        if (this.queue.length > 0 && !this.buffer.updating) {
            this.buffer.appendBuffer(this.queue.shift());
        }
        if (this.video.seekable.length > 0 && this.video.currentTime > 0) {
            if (this.isVideoPlaying()) {
                if (this.videoLoader.style.display != 'none') {
                    this.videoLoader.style.display = 'none';
                    if (this.isAutoRoulette) {
                        $('#videoposterimg').css('display', 'none');
                    }
                }
                $('#videoavailable').hide();
            }
            this.latency = parseFloat((parseFloat(this.video.seekable.end) - parseFloat(this.video.currentTime)).toFixed 2);
            if (this.latency > 2) {
                this.videoLoader.style.display = 'block';
                this.video.currentTime = parseFloat(this.video.seekable.end) - 1;
                this.latency = 0;
            }
            if (+new Date() - this.latencyUpdatedTime > 30000) {
                this.latencyUpdatedTime = +new Date();
                if (this.latency > 0) {
                    window.parent.window.postMessage({
                        'type': 'notifyVideoLatency',
                        'latency': parseFloat((this.latency + 0.75) * 1000).toFixed 0
                    }, '*');
                } else {
                    window.parent.window.postMessage({
                        'type': 'notifyVideoLatency',
                        'latency': 750
                    }, '*');
                }
            }
        } else if (this.video.seekable.length > 0 && this.video.readyState > 0 && !this.isVideoPlaying()) {
            if (this.video.seekable.end > 2) {
                this.video.pause();
                this.video.currentTime = parseFloat(this.video.seekable.end) - 1;
            }
        }
    };
    _0x4898dd.prototype.checkForBandwidthToDowngrade = function () {
        var _0x36e223 = this.getCurrentChannelBasedOnBandwidth();
        if (this.currentChannel == 3 && _0x36e223 == 3) {
            _0x36e223 = -1;
        } else if (this.currentChannel == _0x36e223) {
            _0x36e223 = this.currentChannel + 1;
        }
        if (this.currentChannel != _0x36e223 && _0x36e223 != -1) {
            if (!this.isAutoChannel) {
                this.isAutoChannel = true;
                window.parent.window.postMessage({
                    'type': 'LOW_BANDWIDTH'
                }, '*');
            }
            this.currentChannel = _0x36e223;
            this.resetplayer();
            this.initWebsocketPlayer();
            this.isChannelGoingDown = true;
            this.isChannelChanging = true;
        } else if (_0x36e223 == -1) {}
    };
    _0x4898dd.prototype.isVideoPlaying = function () {
        return !!(this.video.currentTime > 0 && !this.video.paused && !this.video.ended && this.video.readyState > 2);
    };
    _0x4898dd.prototype.getCurrentTimeStamp = function () {
        var _0x246e5d = new Date();
        var _0x28d3bd = _0x246e5d.getTime();
        return _0x28d3bd;
    };
    _0x4898dd.prototype.resetplayer = function () {
        if (this.ws) {
            this.ws.close();
            this.ws = null;
        }
        this.buffer = null;
        this.initBuffer = new Uint8Array();
        this.activeConnectionTime = +new Date();
        this.isLowBandWidth = false;
        this.isPlayerStarted = false;
        this.queue = [];
        this.bandwidthArray = [];
        this.isAutoChannel = true;
        this.isInitBandWidth = false;
        this.clientBandwidth = 0;
        this.avgbandwidth = 0;
        this.bandwidth = 0;
        this.latency = 0;
        this.videoPauseCount = 0;
        this.channelChangeTime = +new Date();
        this.videoPauseCountResetTime = +new Date();
        if (this.channelUptimer && this.worker == null) {
            clearInterval(this.channelUptimer);
            this.channelUptimer = null;
            var _0x11fa5 = this;
            this.channelUptimer = setInterval(function () {
                _0x11fa5.checkForChannelUp();
            }, 60000);
        }
        this.bwPrintCounter = 0;
        this.isChannelChanging = false;
        this.latencyUpdatedTime = +new Date();
        window.parent.window.postMessage({
            'type': 'notifyVideoLatency',
            'latency': 2500
        }, '*');
        this.isChannelGoingDown = false;
        this.isFirstInitBuffer = true;
        if (this.videoWaitingTimer) {
            clearTimeout(this.videoWaitingTimer);
            this.videoWaitingTimer = null;
        }
        this.videoLoader.style.display = 'block';
        if (this.isAutoRoulette) {
            $('#videoposterimg').css('display', 'block');
        }
        this.avgInstantBandwidthArray = [];
        this.avgBandwidth = 0;
        if (this.reconnectingTimer) {
            clearInterval(this.reconnectingTimer);
            this.reconnectingTimer = null;
        }
        this.pingCounter = 0;
        this.pingReturnCounter = 0;
        this.pingDowngradeCount = 0;
        if (this.pingTimer) {
            clearInterval(this.pingTimer);
            this.pingTimer = null;
        }
        if (this.pingTimerTimeout) {
            clearTimeout(this.pingTimerTimeout);
            this.pingTimerTimeout = null;
        }
        if (this.video && !this.IsSafari) this.context.drawImage(this.video, 0, 0, 640, 360);
        this.img = new Image();
        this.img.src = this.posterUrl + '?n=' + Math.random();
        this.img.onload = () => {
            this.videoLoader.style.display = 'block';
            $('#videoposterimg').attr('src', this.img.src);
        };
        if (this.isAutoRoulette) {
            $('#videoposterimg').css('display', 'block');
        }
        this.isVideoPlaybackStarted = false;
        this.playbackStartTime = null;
        this.pingPongArray = [];
        this.latencyArray = [];
    };
    _0x4898dd.prototype.calculateBandwidth = function (_0x4adc9d) {
        if (this.bandwidthArray.length > 20) {
            this.bandwidthArray.shift();
        }
        this.bandwidthArray.push({
            'ts': +new Date(),
            'd': _0x4adc9d
        });
        var _0x43b4d2 = (this.bandwidthArray[this.bandwidthArray.length - 1].ts - this.bandwidthArray[0].ts) / 1000;
        var _0x1f0a03 = 0;
        for (var _0x457e2b = 0; _0x457e2b < this.bandwidthArray.length; _0x457e2b++) {
            _0x1f0a03 += this.bandwidthArray[_0x457e2b].d;
        }
        if (_0x43b4d2 != 0) {
            this.avgbandwidth = _0x1f0a03 / _0x43b4d2 / 1000;
        }
        if (this.bwPrintCounter % 20 == 0) {}
        this.bwPrintCounter++;
    };
    _0x4898dd.prototype.getChannel = function (_0x53dcf2, _0x1afd1c) {
        if (_0x1afd1c == 'up') {
            var _0x3a44bb = this.clientBandwidth;
            if (_0x3a44bb > this.bandwidthDownloadSize[0]) {
                return 0;
            } else if (_0x3a44bb > this.bandwidthDownloadSize[1]) {
                return 1;
            } else if (_0x3a44bb > this.bandwidthDownloadSize[2]) {
                return 2;
            } else {
                return 3;
            }
        }
    };
    _0x4898dd.prototype.getChannelForUpgrade = function (_0xda9d1a) {
        var _0x7a404e = this;
        var _0x3fcffc = this.pingPongArray.findIndex(function (_0x3c3a1d) {
            return _0x3c3a1d > _0x7a404e.pingPongDelayCheckForUpgrade;
        });
        var _0x4599a7 = this.latencyArray.findIndex(function (_0x3632b1) {
            return _0x3632b1 < _0x7a404e.lowLatencyBufferCheckForUpgrade;
        });
        this.pingPongArray = [];
        this.latencyArray = [];
        if (_0x3fcffc < 0 && _0x4599a7 < 0 && this.currentChannel > 0) {
            var _0x1766ac = this.currentChannel - 1;
            return _0x1766ac;
        }
    };
    _0x4898dd.prototype.getCurrentChannelBasedOnBandwidth = function () {
        var _0x24adc9 = this.avgbandwidth;
        if (_0x24adc9 > this.bandwidthDownloadSize[0]) {
            return 0;
        } else if (_0x24adc9 > this.bandwidthDownloadSize[1]) {
            return 1;
        } else if (_0x24adc9 > this.bandwidthDownloadSize[2]) {
            return 2;
        } else {
            return 3;
        }
    };
    _0x4898dd.prototype.appendtwoBuffers = function (_0x5a3dbc, _0x2d483e) {
        var _0x151ff8 = new Uint8Array(_0x5a3dbc.byteLength + _0x2d483e.byteLength);
        _0x151ff8.set(new Uint8Array(_0x5a3dbc), 0);
        _0x151ff8.set(new Uint8Array(_0x2d483e), _0x5a3dbc.byteLength);
        return _0x151ff8;
    };
    _0x4898dd.prototype.onChannelChanged = function (_0x34d29c) {
        if (this.isPlayerStarted) {
            if (_0x34d29c == 0) {
                if (this.currentChannel != 0) this.onChannel1Selected();
                this.isAutoChannel = false;
            } else if (_0x34d29c == 1) {
                if (this.currentChannel != 1) this.onChannel2Selected();
                this.isAutoChannel = false;
            } else if (_0x34d29c == 2) {
                if (this.currentChannel != 2) this.onChannel3Selected();
                this.isAutoChannel = false;
            } else if (_0x34d29c == 3) {
                if (this.currentChannel != 3) this.onChannel4Selected();
                this.isAutoChannel = false;
            } else if (_0x34d29c == 'auto') {
                this.onAutoChannelSelected();
                this.isAutoChannel = true;
            }
        } else {
            if (_0x34d29c == 'auto') {
                this.isAutoChannel = true;
            } else {
                this.isAutoChannel = false;
                this.currentChannel = _0x34d29c;
            }
        }
    };
    _0x4898dd.prototype.onChannel1Selected = function () {
        this.currentChannel = 0;
        this.ShiftChannel();
    };
    _0x4898dd.prototype.onChannel2Selected = function () {
        this.currentChannel = 1;
        this.ShiftChannel();
    };
    _0x4898dd.prototype.onChannel3Selected = function () {
        this.currentChannel = 2;
        this.ShiftChannel();
    };
    _0x4898dd.prototype.onChannel4Selected = function () {
        this.currentChannel = 3;
        this.ShiftChannel();
    };
    _0x4898dd.prototype.onAutoChannelSelected = function () {
        this.isAutoChannel = true;
    };
    _0x4898dd.prototype.ShiftChannel = function () {
        if (!this.isChannelChanging) {
            if (this.worker == null) {
                if (this.videoRestrictToChannel > 0) {
                    if (this.videoRestrictToChannel > this.currentChannel) {
                        this.currentChannel = this.videoRestrictToChannel;
                    }
                }
                this.ws.send('quality' + this.currentChannel);
                this.sendBitrateToYouboraPlugin();
                if (this.channelUptimer) {
                    clearInterval(this.channelUptimer);
                    this.channelUptimer = null;
                    var _0x117a5a = this;
                    this.channelUptimer = setInterval(function () {
                        _0x117a5a.checkForChannelUp();
                    }, 60000);
                }
            }
            this.isChannelChanging = true;
        }
    };
    _0x4898dd.prototype.sendBitrateToYouboraPlugin = function () {
        if (!this.isYouBoraloaded) return;
        if (this.currentChannel == 0) {
            plugin.setOptions({
                'content.bitrate': 2.5e+6,
                'content.rendition': 'hd'
            });
        } else if (this.currentChannel == 1) {
            plugin.setOptions({
                'content.bitrate': 1e+6,
                'content.rendition': 'high'
            });
        } else if (this.currentChannel == 2) {
            plugin.setOptions({
                'content.bitrate': 5e+5,
                'content.rendition': 'medium'
            });
        } else if (this.currentChannel == 3) {
            plugin.setOptions({
                'content.bitrate': 2.5e+5,
                'content.rendition': 'low'
            });
        }
    };
    _0x4898dd.prototype.loadYouBora = function (_0x5b2ec7, _0x16ede8, _0x312f8a, _0x45efec, _0x2e259a, _0xead4da) {
        let _0x177662;
        let _0x43fd08;
        if (_0x45efec) {
            _0x177662 = 'pragmaticplay';
            _0x43fd08 = 'arunpplive';
        } else {
            _0x177662 = 'pragmaticplaydev';
            _0x43fd08 = 'apurushothamdev';
        }
        youbora.Log.logLevel = youbora.Log.Level.NONE;
        if (typeof youbora.adapters.Html5 != 'undefined') {
            window.plugin = new youbora.Plugin({
                'accountCode': _0x177662,
                'user.name': _0x5b2ec7,
                'parse.cdnNode': true,
                'content.cdn': 'Amazon',
                'content.title': _0xead4da,
                'content.customDimension.1': _0x16ede8 ? _0x16ede8.toString() : '',
                'content.customDimension.2': _0x312f8a ? _0x312f8a.toString() : '',
                'content.customDimension.3': _0x5b2ec7 ? _0x5b2ec7.toString() : '',
                'content.isLive': true,
                'content.isLive.noSeek': true,
                'content.duration': 0
            });
            plugin.setAdapter(new youbora.adapters.Html5('video'));
            this.isYouBoraloaded = true;
            this.sendBitrateToYouboraPlugin();
        }
    };
    return _0x4898dd;
}();