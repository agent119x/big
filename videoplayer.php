<?php
$roomno = $_GET["m"];
$stream = "";

switch($roomno) {

    case "A01": $stream = "wss://ws.pragmaticplaylive.net/BC4.1?JSESSIONID=IGvvRr1qvdAnpSH35a6JatZ8UVJNtLmlAgAMS5vlw_49SRaQRued!-1878203830"; break;
    case "EV2": $stream = "ws://203.245.30.130:9702"; break;
    case "EV3": $stream = "ws://203.245.30.130:9703"; break;
    case "EV4": $stream = "ws://203.245.30.130:9704"; break;
    case "EV5": $stream = "ws://203.245.30.130:9705"; break;
    case "EV6": $stream = "ws://203.245.30.130:9706"; break;
    case "EV7": $stream = "ws://203.245.30.130:9707"; break;
}
?>
<html><head>
   <meta charset="utf-8">
   <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
  <link href="https://client.pragmaticplaylive.net/desktop/assets/video/videoplayer.css" rel="stylesheet">
</head>

<body onload="onInit()" style="margin:0 ; padding: 0;outline: none;border: none">
	
    <div id="videoavailable" style="display: none"><div></div></div>
    <img id="videoposterimg">
    <canvas id="videoposter" class="videoposter"></canvas>
    <video class="video" muted="" id="video" width="100%" height="100%" controls="false" data-current-player="true" playsinline="true" webkit-playsinline="true" hidden=""></video>
    
    <div id="videoLoader" class="vPreloader">
      <img src="../../assets/loaderimages/loading-indicator@3x.png" class="loadingIndicator">
      <img src="../../assets/loaderimages/icon-video.svg" class="icon_video">
    </div>

  <script src="https://client.pragmaticplaylive.net/desktop/assets/js/jquery-3.5.1.min.js"></script>
  <script src="./videoplayer.js"></script>
  <script src="https://smartplugin.youbora.com/v6/js/adapters/html5/6.5.5/sp.min.js"></script>
<script>

function onInit()
{
  var vC = new VideoComponent("<?php echo $stream; ?>");
  vC.ngOnInit();
  vC.videoVolume = 0;
}

</script>

</body>
</html>